let allMushroom = [];

// Filter varaibles
let hat = "";
let petals = "";
let body = "";
let color = "";
let searchKeyword = "";

function handleLevel1Click(hatValue) {
    hat = hatValue;
    document.getElementById("petalsBox").style.display = "flex";
    searchMultiLevelMushroom();
}

function handleLevel2Click(petalsValue) {
    petals = petalsValue;
    document.getElementById("bodyBox").style.display = "flex";
    searchMultiLevelMushroom();
}

function handleLevel3Click(bodyValue) {
    body = bodyValue;
    document.getElementById("colorBox").style.display = "flex";
    searchMultiLevelMushroom();
}
function handleLevel4Click(colorValue) {
    color = colorValue;
    searchMultiLevelMushroom();
}

function multiLevelFilter() {

}

function handleLoadEnneMetsaButtonClick() {
    $("#enneMetsa").modal("show");
}

function handleLoadPealeMetsaButtonClick() {
    $("#pealeMetsa").modal("show");
}

function handleLoadMushroomButtonClick() {
    $("#seenteAndmebaas").modal("show");
    loadMushroom();
}

function handleLoadSeenteTootlusButtonClick() {
    $("#seenteTootlus").modal("show");
}


function loadMushroom() {
    hat = "";
    petals = "";
    body = "";
    color = "";
    searchKeyword = "";
    document.getElementById("petalsBox").style.display = "none";
    document.getElementById("bodyBox").style.display = "none";
    document.getElementById("colorBox").style.display = "none";

    fetchMushroom().then(
        function (seened) {
            allMushroom = seened;
            displayMushroom(allMushroom);

        }
    );
}

function displayMushroom(seened) {
    let seentetabelHtml = "";
    for (let i = 0; i < seened.length; i++) {
        seentetabelHtml = seentetabelHtml + ` 
                    <div class="row">
                        <div class="card-body mushroom-card" style="border: 1px solid black;">
                            <h1>${seened[i].nameEe}</h1>
                            <h4>${seened[i].nameLd}</h4>
                            <div> <img src="${seened[i].picture}" style = "width:40%;height:40%;" align="right"/></div>
                            <div>
                            <h3 style="display:inline">Selts: </h3><p style="display:inline">${seened[i].family}</p>
                            </div>
                            <div>
                            <h3 style="display:inline">Värvus: </h3><p style="display:inline">${seened[i].color}</p>
                            </div>
                            <div>
                            <h3 style="display:inline">Viljakeha: </h3><p style="display:inline">${seened[i].body}</p>
                            </div>
                            <div>
                            <h3 style="display:inline">Kübar: </h3><p style="display:inline">${seened[i].hat}</p>
                            </div>
                            <div>
                            <h3 style="display:inline">Seeneliha: </h3><p style="display:inline">${seened[i].meat}</p>
                            </div>
                            <div>
                            <h3 style="display:inline">Eosed: </h3><p style="display:inline">${seened[i].petals}</p>
                            </div>
                            <div>
                            <h3 style="display:inline">Kasvukoht: </h3><p style="display:inline"> ${seened[i].habitat}</p>
                            </div>
                            <div>
                            <h3 style="display:inline">Esinemisaeg: </h3><p style="display:inline">${seened[i].harvest}</p>
                            </div>
                            <div>
                            <h3 style="display:inline">Töötlemine: </h3><p style="display:inline">${seened[i].preparation}</p>
                            </div>
                            <div>
                            <h3 style="display:inline">Säilitamine: </h3><p style="display:inline"> ${seened[i].preservation}</p>
                            </div>
                        </div>
                    </div>
                `;

    }

    document.getElementById("mushroomFilteredList").innerHTML = seentetabelHtml;

}

function handleSearchkeyChange() {
    hat = "";
    petals = "";
    body = "";
    color = "";
    searchKeyword = "";
    document.getElementById("petalsBox").style.display = "none";
    document.getElementById("bodyBox").style.display = "none";
    document.getElementById("colorBox").style.display = "none";
    let searchKey = document.getElementById("searchKey").value;
    let filteredMushroom = searchMushroom(searchKey);
    displayMushroom(filteredMushroom);
}

function searchMushroom(searchKeyword) {
    let resultingMushroom = [];

    for (let i = 0; i < allMushroom.length; i++) {
        if (allMushroom[i].nameEe.toLowerCase().search(searchKeyword.toLowerCase()) != -1) {
            resultingMushroom.push(allMushroom[i]);
        }
    }

    return resultingMushroom;
}

function searchMultiLevelMushroom() {
    let searchResultMushrooms = allMushroom;

    if (hat !== "") {
        searchResultMushrooms = searchByHat(searchResultMushrooms);
    }

    if (petals !== "") {
        searchResultMushrooms = searchByPetals(searchResultMushrooms);
    }

    if (body !== "") {
        searchResultMushrooms = searchByBody(searchResultMushrooms);
    }

    if (color !== "") {
        searchResultMushrooms = searchBycolor(searchResultMushrooms);
    }
    displayMushroom(searchResultMushrooms);
}

function searchByHat(mushrooms) {
    let resultingMushrooms = [];
    for (let i = 0; i < mushrooms.length; i++) {
        if (mushrooms[i].hat.search(hat) != -1) {
            resultingMushrooms.push(mushrooms[i]);
        }
    }
    return resultingMushrooms;
}

function searchByPetals(mushrooms) {
    let resultingMushrooms = [];
    for (let i = 0; i < mushrooms.length; i++) {
        if (mushrooms[i].petals.search(petals) != -1) {
            resultingMushrooms.push(mushrooms[i]);
        }
    }
    return resultingMushrooms;
}

function searchByBody(mushrooms) {
    let resultingMushrooms = [];
        for (let i = 0; i < mushrooms.length; i++) {
            if (mushrooms[i].body.search(body) != -1) {
                resultingMushrooms.push(mushrooms[i]);
            }
        }
        return resultingMushrooms;
    }

function searchBycolor(mushrooms) {
    let resultingMushrooms = [];
        for (let i = 0; i < mushrooms.length; i++) {
            if (mushrooms[i].color.search(color) != -1) {
                resultingMushrooms.push(mushrooms[i]);
            }
        }
        return resultingMushrooms;
}

function handleSave() {
    if (isFormValid() === false) {
        return;
    }
    if (document.getElementById("id").value > 0) {
        handleEdit();
    } else {
        handleAdd();
    }
}


